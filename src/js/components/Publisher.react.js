var React = require('react');
var Link = require('react-router').Link;
var PublisherActions = require('../actions/PublisherActions');

module.exports = React.createClass({
  displayName: 'Publisher',

  _deletePublisher: function () {
    PublisherActions.deletePublisher(this.props.publisher);
  },

  render: function () {
    var publisher = this.props.publisher,
        updatePath = '/publishers/update/' + publisher.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{publisher.name}</td>
        <td>{publisher.email}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deletePublisher}>Delete</a>
        </td>
      </tr>
    );
  }
});