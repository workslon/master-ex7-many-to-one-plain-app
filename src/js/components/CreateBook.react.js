var React = require('react');
var ReactDOM = require('react-dom');
var BookActions = require('../actions/BookActions');
var StatusConstants = require('../constants/StatusConstants');
var PublisherStore = require('../stores/PublisherStore');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'CreateBook',

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      var refs = this.refs;
      refs.isbn.value = refs.title.value = refs.year.value = refs.publisher.value = '';
    }
  },

  _createBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};
    var publisher = refs.publisher || {};

    BookActions.createBook({
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value),
      publisher: {
        objectId: publisher.value
      }
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;
    var publishers = PublisherStore.getAllPublishers();

    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label forHtml="isbn">ISBN</label>
          <input defaultValue="" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
          {errors.isbn && <span className="text-danger">{errors.isbn}</span>}
        </div>
        <div className="form-group">
          <label forHtml="title">Title</label>
          <input defaultValue="" ref="title" type="text" className="form-control" id="title" placeholder="Title" />
          {errors.title && <span className="text-danger">{errors.title}</span>}
        </div>
        <div className="form-group">
          <label forHtml="year">Year</label>
          <input defaultValue="" ref="year" type="text" className="form-control" id="year" placeholder="Year" />
          {errors.year && <span className="text-danger">{errors.year}</span>}
        </div>
        <div className="form-group">
          <label forHtml="year">Publishers</label>
          <select className="form-control" ref="publisher">
            <option value="">-No Publisher-</option>
            {publishers.map(function (publisher) {
              return <option value={publisher.objectId} key={publisher.objectId}>{publisher.name} - {publisher.email}</option>
            })}
          </select>
        </div>
        <button type="submit" onClick={this._createBook} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});