var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var BookActions = require('../actions/BookActions');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';
var publishers = [];

var PublisherStore = Object.assign({}, EventEmitter.prototype, {
  getPublisher: function (id) {
    return publishers.filter(function (publisher) {
      return publisher.objectId === id;
    })[0];
  },

  getAllPublishers: function (type) {
    return publishers;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all publishers
    case AppConstants.REQUEST_PUBLISHERS_SUCCESS:
      try {
        publishers = JSON.parse(action.result).results;
        PublisherStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create publisher Success
    case AppConstants.PUBLISHER_SAVE_SUCCESS:
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        publishers.push(action.data);
        PublisherStore.emitChange();
      } catch (e) {}
      break;

    // --- Update publisher Success
    case AppConstants.PUBLISHER_UPDATE_SUCCESS:
      publishers = publishers.map(function (publisher) {
        if (publisher.objectId === action.data.objectId) {
          publisher.name = action.data.name;
        }
        return publisher;
      });

      setTimeout(BookActions.getBooks, 0);

      PublisherStore.emitChange();
      break;

    // -- Destroy publisher Success
    case AppConstants.PUBLISHER_DESTROY_SUCCESS:
      publishers = publishers.filter(function (publisher) {
        return publisher.objectId !== action.data.objectId;
      });

      setTimeout(BookActions.getBooks, 0);

      PublisherStore.emitChange();
      break;
  }
});

module.exports = PublisherStore;