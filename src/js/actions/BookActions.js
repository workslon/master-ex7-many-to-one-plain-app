var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var validator = require('validator');
var XHR = require('xhr-promise');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Book/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getBooks: function () {
    var promise = xhr.GET({
      url: 'https://public-library-api.herokuapp.com/api/classes/Book/?include=publisher',
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOKS,
      success: AppConstants.REQUEST_BOOKS_SUCCESS,
      failure: AppConstants.REQUEST_BOOKS_ERROR
    });

    return promise;
  },

  createBook: function (data) {
    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"isbn":"' + data.isbn + '"}'
      })
      .then(function (result) {
          try {
            if (JSON.parse(result).results.length) {
              AppDispatcher.dispatch({
                type: AppConstants.NON_UNIQUE_ISBN
              });
            } else {
              if (data.publisher && data.publisher.objectId) {
                data.publisher.__type = 'Pointer';
                data.publisher.className = 'Publisher';
              } else {
                data.publisher = undefined;
              }

              AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
                request: AppConstants.REQUEST_BOOK_SAVE,
                success: AppConstants.BOOK_SAVE_SUCCESS,
                failure: AppConstants.BOOK_SAVE_ERROR
              }, data);
            }
          } catch(e) {}
        },

        function (error) {
          alert('Server error!');
        }
      );
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  updateBook: function (book, newData) {
    var promise;

    validator.validate(newData);

    if (validator.isValid()) {
      if (newData.publisher && newData.publisher.objectId) {
        newData.publisher.__type = 'Pointer';
        newData.publisher.className = 'Publisher';
      } else {
        newData.publisher = {
          __op: 'Delete'
        }
      }

      promise = xhr.PUT({
        url: xhr.defaults.url + book.objectId,
        data: newData
      });

      newData.objectId = book.objectId;

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_BOOK_UPDATE,
        success: AppConstants.BOOK_UPDATE_SUCCESS,
        failure: AppConstants.BOOK_UPDATE_ERROR
      }, newData);
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  deleteBook: function (book) {
    var promise = xhr.DELETE({
      url: xhr.defaults.url + book.objectId
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOK_DESTROY,
      success: AppConstants.BOOK_DESTROY_SUCCESS,
      failure: AppConstants.BOOK_DESTROY_ERROR
    }, book)
  }
};