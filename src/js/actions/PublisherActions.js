var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var validator = require('validator');
var XHR = require('xhr-promise');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Publisher/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getPublishers: function () {
    var promise = xhr.GET();

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_PUBLISHERS,
      success: AppConstants.REQUEST_PUBLISHERS_SUCCESS,
      failure: AppConstants.REQUEST_PUBLISHERS_ERROR
    });

    return promise;
  },

  createPublisher: function (data) {
    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"email":"' + data.email + '"}'
      })
      .then(function (result) {
          try {
            if (JSON.parse(result).results.length) {
              AppDispatcher.dispatch({
                type: AppConstants.NON_UNIQUE_EMAIL
              });
            } else {
              AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
                request: AppConstants.REQUEST_PUBLISHER_SAVE,
                success: AppConstants.PUBLISHER_SAVE_SUCCESS,
                failure: AppConstants.PUBLISHER_SAVE_ERROR
              }, data);
            }
          } catch(e) {}
        },

        function (error) {
          alert('Server error!');
        }
      );
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.PUBLISHER_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  updatePublisher: function (publisher, newData) {
    var promise;

    validator.validate(newData);

    if (validator.isValid()) {
      promise = xhr.PUT({
        url: xhr.defaults.url + publisher.objectId,
        data: newData
      });

      newData.objectId = publisher.objectId;

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_PUBLISHER_UPDATE,
        success: AppConstants.PUBLISHER_UPDATE_SUCCESS,
        failure: AppConstants.PUBLISHER_UPDATE_ERROR
      }, newData);
    } else {
      AppDispatcher.dispatch({
        type: 'PUBLISHER_VALIDATION_ERROR',
        errors: validator.errors
      });
    }
  },

  deletePublisher: function (publisher) {
    var promise = xhr.DELETE({
      url: xhr.defaults.url + publisher.objectId
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_PUBLISHER_DESTROY,
      success: AppConstants.PUBLISHER_DESTROY_SUCCESS,
      failure: AppConstants.PUBLISHER_DESTROY_ERROR
    }, publisher)
  }
};